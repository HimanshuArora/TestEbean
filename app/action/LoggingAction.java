package action;

import akka.actor.ActorRef;
import kafka.ActorFactory;
import kafka.KafkaProducerActor;
import play.Configuration;
import play.Logger;
import play.libs.Json;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import kafka.LogUtility;

import javax.inject.Inject;
import java.lang.reflect.Method;
import java.util.concurrent.CompletionStage;

public class LoggingAction implements play.http.ActionCreator  {

    @Inject
    private Configuration config;
    @Inject
    private ActorFactory actorCreator;
    @Inject
    private LogUtility logUtility;

    @Override
    public Action createAction(Http.Request request, Method actionMethod) {
        Logger.info("<<< {} : {} {} {}", logUtility.getCaller(request.getHeader("access_token")), request.method(), request.uri(), Json.stringify(request.body().asJson()));
        if (logUtility.isKafkaEnabled()) {
            actorCreator.getKafkaProducerActorRef().tell(new KafkaProducerActor.LogMessage(logUtility.toJson(request)), ActorRef.noSender());
        }
        return new Action.Simple() {
            @Override
            public CompletionStage<Result> call(Http.Context ctx) {
                return delegate.call(ctx);
            }
        };
    }

}
