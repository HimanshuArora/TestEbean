package kafka;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import play.Configuration;
import play.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Properties;

/**
 * Created by J16DL00 on 18/01/2017.
 */
@Singleton
public class KafkaLogProducer {

    private Configuration configuration;
    private String topicName;
    private Properties props;
    private Producer producer;
    private Boolean enable;

    @Inject
    public KafkaLogProducer(Configuration configuration) {
        this.configuration = configuration;
        props = new Properties();
        props.put("bootstrap.servers", configuration.getString("kafka.metadata.broker.list"));
        props.put("key.serializer", configuration.getString("kafka.serializer.class"));
        props.put("value.serializer", configuration.getString("kafka.serializer.class"));
        props.put("acks", configuration.getString("kafka.acks"));
        props.put("reconnect.backoff.ms", configuration.getString("kafka.reconnect.backoff.ms"));
        topicName = configuration.getString("kafka.topicName");
        enable = configuration.getBoolean("kafka.log.enable");
        try {
           if(enable) producer = new org.apache.kafka.clients.producer.KafkaProducer<String, String>(props);
        } catch (Throwable e) {
            Logger.error("Error occured while connecting to kafka cluster " , e);
        }
    }

    public void send(String msg) {
        try {
            if(enable && producer != null) {
                producer.send(new ProducerRecord(topicName, msg));
            } else if(producer != null) {
                producer.close();
            }
        } catch (Throwable e) {
            Logger.error("Error occured while connecting to kafka cluster " , e);
        }
    }
}
