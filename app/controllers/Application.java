package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.User;
import play.libs.Json;
import play.mvc.*;

public class Application extends Controller {

    public Result getUser(String id) {
        JsonNode res = Json.toJson(User.find
                .fetch("addresses")
                .setUseQueryCache(true)
                .where().eq("user_id", Integer.parseInt(id))
                .findList());
        return ok(res);
    }

    public Result getAllUsers() {
        JsonNode res = Json.toJson(User.find
                .fetch("addresses")
                .setUseQueryCache(true)
                .findList());
        return ok(res);
    }
    public Result testPost() {
        return ok();
    }

}
